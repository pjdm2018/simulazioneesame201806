package it.uniroma2.pjdm.simulazioneesame201806a;

import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Handler handler;
    private FrameLayout myLayout;
    private int ballCount = 0;

    private void addBall(int offset_x, int offset_y) {
        ImageView iv = new ImageView(MainActivity.this);
        iv.setImageResource(R.drawable.ball);

        // constrain to 50 dpi
        int imgSize = (int) (50 * getResources().getDisplayMetrics().density);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(imgSize, imgSize);

        // place the imageview
        lp.setMargins(offset_x - imgSize/2, offset_y - imgSize/2, 0,0);

        // add the imageview to the layout
        iv.setLayoutParams(lp);
        myLayout.addView(iv);
        ballCount++;
    }

    private void removeBall(int index) {
        myLayout.removeViewAt(index);
        ballCount--;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if(ballCount > 0) {
                    int randIndex = (int) (Math.random() * ballCount);
                    Log.d("CLA", "removing: " + randIndex + " / " + ballCount);
                    removeBall(randIndex);
                }
                handler.sendMessageDelayed(handler.obtainMessage(), 5000);
            }
        };

        handler.sendMessageDelayed(handler.obtainMessage(), 5000);

        myLayout = findViewById(R.id.myLayout);
        myLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    int offset_x = (int) motionEvent.getX();
                    int offset_y = (int) motionEvent.getY();

                    addBall(offset_x, offset_y);
                }
                return false;
            }
        });
    }
}
